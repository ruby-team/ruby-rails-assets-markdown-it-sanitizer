# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-markdown-it-sanitizer/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-markdown-it-sanitizer"
  spec.version       = RailsAssetsMarkdownItSanitizer::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "sanitizer for markdown-it."
  spec.summary       = "sanitizer for markdown-it."
  spec.homepage      = "https://github.com/svbergerem/markdown-it-sanitizer"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
